# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'crm.configuration'
    region_defect = fields.Many2One('crm.region_fiduprevisora',
        'Region For Defect')
    response_mail_template = fields.Many2One('email.template',
        'Template Response Emails')
    notification_mail_template = fields.Many2One('email.template',
        'Template Notification Emails')
