# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.modules.dash.dash import DashAppBase


class DashApp(metaclass=PoolMeta):
    __name__ = 'dash.app'

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend([
            'dash.app.crm_fiduprevisora',
            'dash.app.consulta_pqr'
        ])
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend([
            ('crm_fiduprevisora', 'Crm Fiduprevisora'),
            ('consulta_pqr', 'Cosulta PQR'),
        ])
        return options


class AppConsultaPQR(DashAppBase):
    "App Consulta PQR"
    __name__ = "dash.app.consulta_pqr"
