# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool, Not


class Case(metaclass=PoolMeta):
    __name__ = "crm.case"
    macroprocess = fields.Char('Macroprocess')
    process_service = fields.Char('Process Service')
    motives_health = fields.Char('Motives General Health')
    type_request = fields.Char('Type Request')
    priority = fields.Char('Priority')
    time_max = fields.Numeric('Time Max Response (days)', digits=(2, 0))
    quality_attribute = fields.Char('Quality Attribute')
    required_attach = fields.Boolean('Required Attach')
    attach_description = fields.Char('Attach Description', states={
        'invisible': (Not(Bool(Eval('required_attach')))),
        'required': (Bool(Eval('required_attach'))),
    })

    def get_rec_name(self, name):
        return self.name
